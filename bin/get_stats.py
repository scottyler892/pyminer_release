#!/usr/bin/env python3

##import dependency libraries
import sys,time,glob,os,pickle,fileinput
from subprocess import Popen
from operator import itemgetter
import gc, fileinput
import numpy as np
import argparse
#import pandas as pd
##############################################################
## basic function library
def read_file(tempFile,linesOraw='lines',quiet=False):
    if not quiet:
        print('reading',tempFile)
    f=open(tempFile,'r')
    if linesOraw=='lines':
        lines=f.readlines()
        for i in range(0,len(lines)):
            lines[i]=lines[i].strip('\n')
    elif linesOraw=='raw':
        lines=f.read()
    f.close()
    return(lines)

def make_file(contents,path):
    f=open(path,'w')
    if isinstance(contents,list):
        f.writelines(contents)
    elif isinstance(contents,str):
        f.write(contents)
    f.close()

    
def flatten_2D_table(table,delim):
    #print(type(table))
    if str(type(table))=="<class 'numpy.ndarray'>":
        out=[]
        for i in range(0,len(table)):
            out.append([])
            for j in range(0,len(table[i])):
                try:
                    str(table[i][j])
                except:
                    print(table[i][j])
                else:
                    out[i].append(str(table[i][j]))
            out[i]=delim.join(out[i])+'\n'
        return(out)
    else:
        for i in range(0,len(table)):
            for j in range(0,len(table[i])):
                try:
                    str(table[i][j])
                except:
                    print(table[i][j])
                else:
                    table[i][j]=str(table[i][j])
            table[i]=delim.join(table[i])+'\n'
    #print(table[0])
        return(table)

def strip_split(line, delim = '\t'):
    return(line.strip('\n').split(delim))

def make_table(lines,delim):
    for i in range(0,len(lines)):
        lines[i]=lines[i].strip()
        lines[i]=lines[i].split(delim)
        for j in range(0,len(lines[i])):
            try:
                float(lines[i][j])
            except:
                lines[i][j]=lines[i][j].replace('"','')
            else:
                lines[i][j]=float(lines[i][j])
    return(lines)


def get_file_path(in_path):
    in_path = in_path.split('/')
    in_path = in_path[:-1]
    in_path = '/'.join(in_path)
    return(in_path+'/')


def read_table(file, sep='\t'):
    return(make_table(read_file(file,'lines'),sep))
    
def write_table(table, out_file, sep = '\t'):
    make_file(flatten_2D_table(table,sep), out_file)
    

def import_dict(f):
    f=open(f,'rb')
    d=pickle.load(f)
    f.close()
    return(d)

def save_dict(d,path):
    f=open(path,'wb')
    pickle.dump(d,f)
    f.close()

def cmd(in_message, com=True):
    print(in_message)
    time.sleep(.25)
    if com:
        Popen(in_message,shell=True).communicate()
    else:
        Popen(in_message,shell=True)



##############################################################

##########################################################################
parser = argparse.ArgumentParser()

## global arguments
parser.add_argument(
	'-infile','-in','-i','-input',
	dest='infile',
	type=str)

parser.add_argument(
	"-sample_groups",
    help='if you know how which samples belong to which groups, feed in a file that has the samples in the first column, and their group number (index starting at 0), in the second column. The IDs must be in the same order as in the infile too.',
 	dest = 'sample_groups',
 	type = str)

parser.add_argument(
    "-classes",
    help='if there are classes to compare, put the annotation table in this argument',
    dest = 'class_file',
    type = str)

parser.add_argument(
    "-within_group",
    help='if you know how which samples belong to which groups, feed in a file that has the samples in the first column, and their group number (index starting at 0), in the second column. The IDs must be in the same order as in the infile too.',
    dest = 'within_group',
    type = int)

parser.add_argument(
	"-out_dir","-o","-out",
    help='if you know how which samples belong to which groups, feed in a file that has the samples in the first column, and their group number (index starting at 0), in the second column. The IDs must be in the same order as in the infile too.',
 	dest = 'out_dir',
 	type = str)

parser.add_argument(
    "-species","-s",
    help = 'what species is this? Must be gProfiler compatible.',
    dest = 'species',
    type = str,
    default = 'hsapiens')

parser.add_argument(
    '-no_gProfile',
    help = 'should we do the automated gprofiler results?',
    default = False,
    action = 'store_true')

parser.add_argument(
	"-FDR","-fdr","-fdr_cutoff",
    help='The desired Benjamini-Hochberg False Discovery Rate (FDR) for multiple comparisons correction (default = 0.05)',
 	dest = 'FDR_cutoff',
 	type = float,
 	default = 0.05)

parser.add_argument(
	"-Zscore","-Z_score_cutoff","-Z","-zscore","-z",
    help='The desired False Discovery Rate (FDR) for multiple comparisons correction (default = 0.05)',
 	dest = 'Zscore',
 	type = float,
 	default = 2.0)

parser.add_argument(
    '-hdf5',
    help = 'The input file is an HDF5 file',
    default = False,
    action = 'store_true')

parser.add_argument(
    "-ID_list","-ids",
    help = 'If we are using an hdf5 file, give the row-wise IDs in this new line delimeted file',
    type = str)

parser.add_argument(
    "-columns","-cols",
    help = 'If we are using an hdf5 file, give the column-wise IDs in this new line delimeted file',
    type = str)

parser.add_argument(
    '-rows',
    help = 'if the samples are in rows, and variables are in columns',
    default = False,
    action = 'store_true')

parser.add_argument(
    "-log",'-log2','-log_transform',
    help='do a log transformation prior to clustering',
    action = 'store_true',
    default = False)

parser.add_argument(
    '-lin_norm',
    help = 'should we normalize the rows before doing the stats?',
    default = False,
    action = 'store_true')



args = parser.parse_args()
##########################################################################
args.infile = os.path.realpath(args.infile)
if args.out_dir == None:
	args.out_dir = get_file_path(args.infile)+'sample_clustering_and_summary/'
if args.out_dir[-1]!='/':
    args.out_dir+='/'

sample_dir = args.out_dir
temp = args.out_dir
cmd('mkdir '+args.out_dir)

##############################################################################
## read in the dataset and sample groups

if args.within_group==None:
    sample_group_table = read_table(args.sample_groups)

    sample_group_table_np = np.array(sample_group_table)
    sample_group_order = np.transpose(sample_group_table_np[:,0])
    sorted_list_of_ids = list(sample_group_order)

    grouping_vector = list(np.transpose(sample_group_table_np[:,1]))
    #print(grouping_vector)
    #sys.exit()
else:
    if args.class_file==None:
        sys.exit("to run within class analysis, we need the classes annotation file: -classes")
    orig_sample_group_table = read_table(args.sample_groups)
    orig_sample_group_table_np = np.array(orig_sample_group_table)
    orig_sample_group_order = np.transpose(orig_sample_group_table_np[:,0])
    orig_sorted_list_of_ids = list(orig_sample_group_order)
    orig_grouping_vector = list(np.transpose(orig_sample_group_table_np[:,1]))
    ## we're going to switch around the normal sample groups with the 
    ## classes that are read in 
    class_table_np = np.array(read_table(args.class_file))
    class_labels = list(set(class_table_np[:,1].tolist()))
    class_labels = sorted(class_labels)
    class_hash = {key:value for value, key in enumerate(class_labels)}
    alias_dict = {key:value for key, value in enumerate(class_labels)}
    ## make a table to define which class is which group
    class_index_table = []
    for c in class_labels:
        class_index_table.append([c,class_hash[c]])
        #print(class_index_table[-1])
    ## figure out which indices to keep
    subset_col_idxs=[]
    for i in range(0,len(orig_grouping_vector)):
        if str(orig_grouping_vector[i])==str(float(args.within_group)):
            subset_col_idxs.append(i)
        #print(orig_grouping_vector[i])
    #print(subset_col_idxs,float(args.within_group))
    sample_group_table_np = class_table_np[subset_col_idxs]
    ## go through the annotation table and make
    for i in range(len(subset_col_idxs)):
        sample_group_table_np[i,1]=class_hash[sample_group_table_np[i,1]]
    #print(sample_group_table_np)

    sample_group_table = sample_group_table_np.tolist()
    for i in range(len(sample_group_table)):
        sample_group_table[i][1]=float(sample_group_table[i][1])
    sample_group_table_np = np.array(sample_group_table)
    sample_group_order = np.transpose(sample_group_table_np[:,0])
    sorted_list_of_ids = list(sample_group_order)
    grouping_vector = list(np.transpose(sample_group_table_np[:,1]))
    subset_col_idxs=np.array(subset_col_idxs,dtype=int)
    #print(grouping_vector)
    


if len(list(set(grouping_vector))) == 1:
	one_group = True
	sys.exit('only one sample group, nothing to calculate')
else:
	one_group = False




sample_cluster_ids = []
for i in range(0,len(sample_group_table)):
    
    ## THIS IS IMPORTANT
    ## here we assume that the samples are all listed in the same order as in '-infile'
    ## we also assume that the group indexing starts at 0
    sample_cluster_ids.append(sample_group_table[i][1])
sample_cluster_ids = list(map(int,sample_cluster_ids))
sample_k_lists = []
for i in range(0,max(sample_cluster_ids)+1):
    sample_k_lists.append([])
#print(len(sample_k_lists))
## now populate the list of lists
for i in range(0,len(sample_cluster_ids)):
    ## this appends the sample index to 
    #print(sample_cluster_ids[i])
    sample_k_lists[sample_cluster_ids[i]].append(i)

#print(sample_k_lists)




## check for formatting
if not args.hdf5:
    full_expression_str = read_table(args.infile)
    title = full_expression_str[0]
    full_expression_np = np.array(full_expression_str)
    row_names = full_expression_np[1:,0]
    full_expression = np.array(full_expression_np[1:,1:],dtype = float)
else:
    row_names = read_file(args.ID_list,'lines')
    title = read_file(args.columns,'lines')
    print('making a maliable hdf5 file to preserve the original data')
    cmd('cp '+args.infile+' '+args.infile+'_copy')
    import h5py
    print('reading in hdf5 file')
    infile_path = args.infile+'_copy'
    h5f = h5py.File(infile_path, 'r+')
    full_expression=h5f["infile"]

##########################################################
## if we're doing the subset analysis, subset which columns to include
if args.within_group!=None:
    full_expression = full_expression[:,subset_col_idxs]
    temp_title = np.array(title[1:])
    title = [title[0]] + temp_title[subset_col_idxs].tolist()
    print(row_names)

##########################################################
if args.rows:
    full_expression = np.transpose(full_expression)
    temp_col = ["variables"] + row_names.tolist()
    title = temp_col[:]


if title[1:] != sorted_list_of_ids:
    print('length of columns',len(title[1:]))
    print('length of sample_groups',sorted_list_of_ids)
    for i in range(1,len(title)):
        if title[i] != sorted_list_of_ids[i-1]:
            print('on line',i,'ID_list',title[i],'',sorted_list_of_ids[i-1])
    sys.exit("the sample group ids have to be in the same order as in the input matrix")



def lin_norm_rows(in_mat,min_range=0,max_range=1):
    in_mat = np.transpose(np.array(in_mat))
    in_mat = in_mat - np.min(in_mat, axis = 0)
    in_mat = in_mat / np.max(in_mat, axis = 0)
    in_mat[np.isnan(in_mat)]=0
    return(np.transpose(in_mat))


## make all of the nans equal to zero
full_expression[np.isnan(full_expression)] = 0.0
if args.log:
    full_expression = np.log2(full_expression-np.min(full_expression)+1)
if args.lin_norm:
    full_expression = lin_norm_rows(full_expression)

IDlist = list(row_names)

##############################################################################
## calculate the sample level Z-scores


## first normalize the expression matrix linearly between 1 and 2
#sample_var_enrichment = lin_norm_rows(full_expression) + 1
if not args.hdf5:
    sample_var_enrichment = full_expression

    ## then calculate the mean for each variable
    norm_row_means = np.transpose(np.array([np.mean(sample_var_enrichment, axis = 1)]))
    print(norm_row_means)
    ## then calculate the sd for each variable
    norm_row_sd = np.transpose(np.array([np.std(sample_var_enrichment, axis = 1)]))
    print(norm_row_sd)
    ## then calculate the delta for each variable
    norm_row_delta = sample_var_enrichment - norm_row_means

    ## calculate the z-score (ie: how many standard deviations away from the mean is each sample)
    sample_var_enrichment_numeric = norm_row_delta/norm_row_sd
    print(sample_var_enrichment_numeric)
    #sys.exit()



    ## add the titles

    sample_var_enrichment = np.array([title[1:]] + sample_var_enrichment_numeric.tolist()).tolist()

    temp_ids = ['variables']+IDlist
    for i in range(0,len(sample_var_enrichment)):
        sample_var_enrichment[i]=[temp_ids[i]]+sample_var_enrichment[i]

    #sample_var_enrichment = np.hstack((row_titles,sample_var_enrichment))

    write_table(sample_var_enrichment, sample_dir+'sample_var_enrichment_Zscores.txt')




##############################################################################
## go through each group and generate mean and sd summaries, then write to file

list_of_k_sample_indices = sample_k_lists
#print(list_of_k_sample_indices)

if not one_group:
    
    sample_names = list(title)[1:]
    #print(sample_names)
    # list_of_k_sample_indices=[]
    
    # for k in sample_k_lists:
    #     print(k)
    #     temp_indices_list=[]
    #     for i in range(0,len(k)):
    #         temp_indices_list.append(sample_names.index(k[i]))
    #     list_of_k_sample_indices.append(temp_indices_list)


    #print(list_of_k_sample_indices)
    list_of_k_sample_indices = np.array(list_of_k_sample_indices)

    ## initialize mean and sd output tables
    #print(list_of_k_sample_indices[0])
    test=full_expression[:,list_of_k_sample_indices[0]]
    #print(np.mean(test.astype('float32'), axis = 1))
    #print(full_expression[:,list_of_k_sample_indices[0]].shape)




    ## calculate the means accross rows
    all_sample_mean = np.transpose(np.array([np.mean(full_expression, axis=1)]))
    all_sample_sd = np.transpose(np.array([np.std(full_expression, axis=1)]))
    k_group_means = np.transpose(np.array([np.mean(full_expression[:,list_of_k_sample_indices[0]], axis=1)]))
    k_group_sd = np.transpose(np.array([np.std(full_expression[:,list_of_k_sample_indices[0]], axis=1)]))


    for k in range(1,len(list_of_k_sample_indices)):
        ## calc row means
        new_mean_col = np.transpose(np.array([np.mean(full_expression[:,list_of_k_sample_indices[k]], axis=1)]))
        k_group_means = np.hstack((k_group_means, new_mean_col))
    
        ## calc sd
        new_sd_col = np.transpose(np.array([np.std(full_expression[:,list_of_k_sample_indices[k]], axis=1)]))
        k_group_sd = np.hstack((k_group_sd, new_sd_col))

##############################################################################
########### use the means of the k-means groups to calculate the enrichment



    ## calculate the sample group z-scores ((Xbar-Mu)/sigma) * sqrt(n), or (Xbar-Mu)/(sigma/sqrt(n))
    print("calculating the sample group vs all sample means")
    print("np.shape(k_group_means)")
    print(np.shape(k_group_means))
    print("np.shape(all_sample_mean)")
    print(np.shape(all_sample_mean))
    row_delta = np.array(k_group_means.transpose() - all_sample_mean.transpose()).transpose()
    print(np.shape(row_delta))
    for k in range(0,len(list_of_k_sample_indices)):
        #print(k)
        row_delta[:,k] = np.array(row_delta[:,k].transpose() / all_sample_sd[:,0].transpose()).transpose()
        row_delta[:,k] = row_delta[:,k] * np.sqrt(len(list_of_k_sample_indices[k]))

    sample_k_group_enrichment = row_delta
    sample_k_group_enrichment_numeric = sample_k_group_enrichment




    #######################################################


    #sample_k_group_enrichment = np.log2(k_group_means_norm / mean_of_group_means)
    print(np.shape(sample_k_group_enrichment))
    print('sample_k_group_enrichment',sample_k_group_enrichment)


    sample_groups = []
    for k in range(0,len(list_of_k_sample_indices)):
        #print(k)
        if 'alias_dict' in globals():
            #print(list(alias_dict.keys()))
            temp_sample_group_name = alias_dict[k]
        else:
            temp_sample_group_name = 'sample_group_'+str(k)
        sample_groups.append(temp_sample_group_name)
        #print(sample_groups)
    sample_groups = np.array(sample_groups)
    print(np.shape(sample_groups))
    #print(sample_groups)

    k_group_means = np.vstack((sample_groups,k_group_means))
    k_group_sd = np.vstack((sample_groups,k_group_sd))
    sample_k_group_enrichment = np.vstack((sample_groups, sample_k_group_enrichment))

    ##############################################################################
    ##### prepare the k-means enrichment for writing to file, then write it
    
    ## these are the names of the variables, making the row labels for the output table
    row_names = np.transpose(np.array([['var_names']+IDlist]))
    
    
    k_group_means = np.hstack((row_names, k_group_means))
    k_group_sd = np.hstack((row_names, k_group_sd))
    sample_k_group_enrichment = np.hstack((row_names, sample_k_group_enrichment))



    write_table(k_group_means,sample_dir+'k_group_means.txt')
    write_table(k_group_sd,sample_dir+'k_group_sd.txt')
    write_table(sample_k_group_enrichment, sample_dir+'k_group_enrichment.txt')
    
    
    print('sample_k_group_enrichment')
    print(sample_k_group_enrichment)
    print(sample_k_group_enrichment[1:,:][:,1:])


##################################################################################################################
##### do a one way anova between groups to test for variables that are significantly different between them ######

from scipy.stats.mstats import f_oneway as aov
from scipy.stats import ttest_ind as ttest

def get_ttests(index, aov_p):
    ## for use in the context of a Friedman's Protected LSD post-hoc to significant anovas
    global args, full_expression, list_of_k_sample_indices
    p_vals = np.ones((len(list_of_k_sample_indices),len(list_of_k_sample_indices)))
    if aov_p > args.FDR_cutoff:
        return(p_vals)
    list_of_group_values=[]
    for group in range(0,len(list_of_k_sample_indices)):
        list_of_group_values.append(full_expression[index,list_of_k_sample_indices[group]])
    for i in range(0,len(list_of_group_values)-1):
        for j in range(i+1, len(list_of_group_values)):
            stat, p = ttest(list_of_group_values[i],list_of_group_values[j])
            p_vals[i,j]=p
            p_vals[j,i]=p
    return(p_vals)

def get_anova(index):
    #print('\n'*10)
    global full_expression, list_of_k_sample_indices
    list_of_group_values=[]
    for group in range(0,len(list_of_k_sample_indices)):
        #print(len(list_of_k_sample_indices[group]))
        list_of_group_values.append(full_expression[index,list_of_k_sample_indices[group]])
        #print(np.shape(list_of_group_values[-1]))
        #print(list_of_group_values[-1])
        
    return_val = list(aov(*list_of_group_values))
    return(return_val)


## this function was adopted from emre's stackoverflow answer found here:
## https://stackoverflow.com/questions/7450957/how-to-implement-rs-p-adjust-in-python
def correct_pvalues_for_multiple_testing(pvalues, correction_type = "Benjamini-Hochberg"):                
    """                                                                                                   
    consistent with R - print correct_pvalues_for_multiple_testing([0.0, 0.01, 0.029, 0.03, 0.031, 0.05, 0.069, 0.07, 0.071, 0.09, 0.1]) 
    """
    from numpy import array, empty
    pvalues = array(pvalues)
    n = int(pvalues.shape[0])
    new_pvalues = empty(n)
    if correction_type == "Bonferroni":
        new_pvalues = n * pvalues
    elif correction_type == "Bonferroni-Holm":
        values = [ (pvalue, i) for i, pvalue in enumerate(pvalues) ]
        values.sort()
        for rank, vals in enumerate(values):
            pvalue, i = vals
            new_pvalues[i] = (n-rank) * pvalue
    elif correction_type == "Benjamini-Hochberg":
        values = [ (pvalue, i) for i, pvalue in enumerate(pvalues) ]
        values.sort()
        values.reverse()
        new_values = []
        for i, vals in enumerate(values):
            rank = n - i
            pvalue, index = vals
            new_values.append((n/rank) * pvalue)
        for i in range(0, int(n)-1):
            if new_values[i] < new_values[i+1]:
                new_values[i+1] = new_values[i]
        for i, vals in enumerate(values):
            pvalue, index = vals
            new_pvalues[index] = new_values[i]
    return new_pvalues


# if not very_big_file:
#     if len(list_of_k_sample_indices) > 1:
#         enough_k_samples = True
#     else:
#         enough_k_samples = False
# else:
#     enough_k_samples = False

print('doing the 1-way ANOVAs')
all_sig_enriched_files=[]
all_sig_enriched_gprof_files = []
FDR_cutoff = args.FDR_cutoff
zscore_cutoff = args.Zscore
if not one_group:
    anova_output = [['Variable', 'F', 'uncorrected_p-val', 'BH_corrected_p-val','-log10(BH_cor_p-val)']]
    aov_uncorrected_p_val_list=[]

## go through each variable and get the uncorrected anova stats
    for i in range(0,len(IDlist)):
        if i % 500==0:
            print('\t',i)
        anova_output.append([IDlist[i]]+get_anova(i))
        aov_uncorrected_p_val_list.append(anova_output[-1][-1])

    ## replace nans with 1s
    aov_uncorrected_p_val_list=np.array(aov_uncorrected_p_val_list)
    aov_uncorrected_p_val_list[np.isnan(aov_uncorrected_p_val_list)] = 1

    print(aov_uncorrected_p_val_list[:10])
    print(correct_pvalues_for_multiple_testing(aov_uncorrected_p_val_list[:10]))
    #for line in anova_output:
    #    print(line)
    ## correct the p-values with Benjamini-Hochberg 
    BH_corrected_aov = correct_pvalues_for_multiple_testing(aov_uncorrected_p_val_list)
    print(BH_corrected_aov[:10])

    indices_less_than_FDR_cutoff=[]
    for i in range(0,len(IDlist)):
        anova_output[i+1]+=[BH_corrected_aov[i], -1*np.log10(BH_corrected_aov[i])]
        if BH_corrected_aov[i] <= FDR_cutoff:
            indices_less_than_FDR_cutoff.append(i)


    cmd('mkdir "'+args.out_dir+'significance"')
    write_table(anova_output , args.out_dir+'significance/groups_1way_anova_results.txt')

    ################################################################################################
    ############# Find the significantly different & enriched variables for each group #############
    ################################################################################################

    ## we start off with the variables that are significantly different between groups
    
    ## this is a list of lists for each 
    group_sig_enriched_bool=[]


    for i in range(0,len(IDlist)):
        ## check to see if this variable was significant in the BH corrected 1-way anova
        if i in indices_less_than_FDR_cutoff:
            var_significant = True
        else:
            var_significant = False
        
        ## if it's significant, we want to go in and check which, if any sample groups 
        ## show an elevated z-score enrichment (greater than the zscore_cutoff variable)
        if var_significant:
        	#print(sample_k_group_enrichment_numeric[i,])
        	temp_sig_enriched_bool_list = list(sample_k_group_enrichment_numeric[i,] >= zscore_cutoff)
        else: 
            temp_sig_enriched_bool_list = [False]*len(list_of_k_sample_indices)
        
        group_sig_enriched_bool.append(temp_sig_enriched_bool_list)
    
    bool_sample_groups = []
    for s in sample_groups:
        bool_sample_groups.append('bool_'+s)
    bool_sample_groups = np.array(bool_sample_groups)
    
    group_sig_enriched_bool = np.array(group_sig_enriched_bool)
    group_sig_enriched = np.vstack((bool_sample_groups,group_sig_enriched_bool))
    group_sig_enriched = np.hstack((row_names,group_sig_enriched))
    
    write_table(group_sig_enriched, args.out_dir+'significance/significant_and_enriched_boolean_table.txt')
    
    ## go through each group, and make a list of the variables that are significant and enriched
    num_sig_enriched = []
    for group in range(0,len(list_of_k_sample_indices)):
        sig_and_enriched_for_this_group = []
        for var in range(0,len(IDlist)):
            if group_sig_enriched_bool[var,group]:
                sig_and_enriched_for_this_group.append(IDlist[var])
        num_sig_enriched.append(len(sig_and_enriched_for_this_group))
        temp_file = args.out_dir+'significance/'+sample_groups[group]+'_significant_enriched.txt'
        all_sig_enriched_files.append(temp_file)
        all_sig_enriched_gprof_files.append(args.out_dir+'significance/gprofiler/'+sample_groups[group]+'_significant_enriched_gprofiler.txt')
        make_file('\n'.join(sig_and_enriched_for_this_group), temp_file)
    
#    print(np.shape(sample_groups))
#    print(np.shape(np.array(num_sig_enriched)))
#    print(num_sig_enriched)
    num_sig_enriched = np.transpose(np.vstack((np.array([sample_groups]),np.array([num_sig_enriched]))))
    
    num_sig_enriched = np.vstack((np.array([['sample_group', 'number_of_significant_and_enriched_vars']]), num_sig_enriched))
    
    write_table(num_sig_enriched, args.out_dir+'significance/number_of_variables_significant_and_enriched_for_each_group.txt')


enriched_in_one_bool = np.sum(group_sig_enriched_bool, axis = 1) != 0
temp_IDs = np.array(IDlist)
IDs_enriched_in_one = temp_IDs[enriched_in_one_bool].tolist()
background = args.out_dir+'significance/background_ids.txt'
make_file('\n'.join(IDlist),background)

if not args.no_gProfile:
    cmd('mkdir '+args.out_dir+'significance/gprofiler/')

    for f in range(0,len(all_sig_enriched_files)):
        temp_gprofile = 'pyminer_gprofile.py -i '+all_sig_enriched_files[f]+' -s '+args.species+' -o '+all_sig_enriched_gprof_files[f]
        temp_gprofile += ' -b '+background
        cmd(temp_gprofile)












